package schedulingsimulator;

import schedulingsimulator.Scheduler.Scheduler;
import schedulingsimulator.Scheduler.Task;

import java.util.ArrayList;

public class RoundRobin extends Scheduler {

    private final int timeWindow;
    int counter = 0;

    public RoundRobin(ArrayList<Task> ReadyQueue, int timeWindow) {
        super(ReadyQueue);
        this.timeWindow = timeWindow;
    }

    @Override
    public void schedule() {
        int timeElapsed = 0;
        int index = 0;
        while (true) {
            Task t = this.ReadyQueue.get(index);
            for (int i = 0; i < timeWindow; i++) {
                t.execute();
                timeElapsed++;
                if (t.isCompleted()) {
                    this.ReadyQueue.remove(index);
                    index--;
                    this.logCompletedTask(t, timeElapsed);
                    counter++;
                    System.out.println("Task(RR): " + counter + "(100)");
                    break;
                }
            }
            index++;
            index %= this.ReadyQueue.size();
        }
    }
}
