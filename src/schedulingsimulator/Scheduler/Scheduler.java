/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schedulingsimulator.Scheduler;

import java.util.ArrayList;

abstract public class Scheduler {

    private int avgResponseTime;
    private int avgWaitingTime;

    protected ArrayList<Task> ReadyQueue;
    private ArrayList<Task> CompletedQueue;

    public Scheduler() {
        this.ReadyQueue = new ArrayList<>();
        this.CompletedQueue = new ArrayList<>();
        this.avgResponseTime = 0;
        this.avgWaitingTime = 0;
    }

    public Scheduler(ArrayList<Task> ReadyQueue) {
        this.ReadyQueue = ReadyQueue;
        this.CompletedQueue = new ArrayList<>();
        this.avgResponseTime = 0;
        this.avgWaitingTime = 0;
    }

    /**
     * This is the method that has to be overriden in order to implement the
     * scheduling algorithm
     */
    abstract public void schedule();

    /**
     * This method is invoked to execute the implementation of schedule. It
     * serves no functional purpose and could be omitted but it is usefuls in
     * terms of semantics (so one can execute the scheduling algorithm by simply
     * calling execute)
     */
    public void execute() {
        this.schedule();
    }

    /**
     * This method registers the completed tasks in the CompletedQueue and
     * calculates its response and waiting time
     *
     * @param t the task that is to be added in the CompletedQueue, the
     * completed task
     * @param executionFinishTime the time at which the scheduler finished the
     * t's execution
     * @see Task
     */
    public void logCompletedTask(Task t, int executionFinishTime) {
        t.setResponseAndWaitingTime(executionFinishTime);
        this.CompletedQueue.add(t);
    }

    /**
     * This method goes through the tasks in the CompletedQueue and calculates
     * the system's average response time based on the specific scheduling plan
     * for the set of tasks at hand
     */
    public void generateAvgResponseTime() {
        this.avgResponseTime = 0;
        for (Task t : this.CompletedQueue) {
            this.avgResponseTime += t.getResponseTime();
        }
        this.avgResponseTime = this.avgResponseTime / this.CompletedQueue.size();
    }

    public void generateAvgWaitingTime() {
        this.avgWaitingTime = 0;
        for (Task t : this.CompletedQueue) {
            this.avgWaitingTime += t.getWaitingTime();
        }
        this.avgWaitingTime = this.avgWaitingTime / this.CompletedQueue.size();
    }

    public int getAvgResponseTime() {
        this.generateAvgResponseTime();
        return this.avgResponseTime;
    }

    public int getAvgWaitingTime() {
        this.generateAvgWaitingTime();
        return this.avgWaitingTime;
    }

}
