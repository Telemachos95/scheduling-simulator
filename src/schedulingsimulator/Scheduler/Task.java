/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schedulingsimulator.Scheduler;

import java.util.Comparator;

public class Task {

    private static int Counter = 0;
    private final int workload;
    private int remainingWorkload;
    private final int id;
    private final int priority;
    private final int creationTime;   //the time that the task was first inserted in the Ready Queue
    private int waitingTime;
    private int responseTime;

    public Task(int workload) {
        this.workload = workload;
        this.remainingWorkload = workload;
        this.id = Task.getCounter();
        this.increaseCounter();
        this.priority = -1;
        this.creationTime = 0;
        this.waitingTime = 0;
        this.responseTime = 0;
    }

    public static int getCounter() {
        return Task.Counter;
    }

    private void increaseCounter() {
        Task.Counter++;
    }

    public void execute() {
        this.remainingWorkload--;
    }

    public int getWorkload() {
        return this.workload;
    }

    public int getRemainingWorkload() {
        return this.remainingWorkload;
    }

    public int getCreationTime() {
        return this.creationTime;
    }

    protected void setResponseAndWaitingTime(int executionFinishTime) {
        this.responseTime = executionFinishTime - this.creationTime;
        this.waitingTime = executionFinishTime - this.creationTime - this.workload;
    }

    public int getResponseTime() {
        return this.responseTime;
    }

    public int getPriority() {
        return this.priority;
    }

    public int getWaitingTime() {
        return this.waitingTime;
    }

    public boolean isCompleted() {
        return this.remainingWorkload == 0;
    }

    public int getId() {
        return this.id;
    }

    public static Comparator orderByRemainingWorkload() {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.remainingWorkload - o2.remainingWorkload;
            }
        };
    }

    public static Comparator orderByPriority() {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.priority - o2.priority;
            }
        };
    }
}
