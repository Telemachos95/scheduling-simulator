package schedulingsimulator;

import schedulingsimulator.Scheduler.Scheduler;
import schedulingsimulator.Scheduler.Task;

import java.util.ArrayList;
import java.util.Collections;

public class SRTF extends Scheduler {

    int counter = 0;

    public SRTF(ArrayList<Task> ReadyQueue) {
        super(ReadyQueue);
    }

    @Override
    public void schedule() {
        Collections.sort(this.ReadyQueue, Task.orderByRemainingWorkload());
        int timeElapsed = 0;
        while (true) {
            Task t = this.ReadyQueue.get(0);
            t.execute();
            timeElapsed++;
            if (t.isCompleted()) {
                this.ReadyQueue.remove(0);
                this.logCompletedTask(t, timeElapsed);
                counter++;
                System.out.println("Task(SRTF): " + counter + "(100)");
                break;
            }
        }

    }

}
