package schedulingsimulator.Runnables;

import java.util.ArrayList;
import static schedulingsimulator.GUI.Gui.algorithmList;
import schedulingsimulator.Scheduler.Scheduler;
import schedulingsimulator.Scheduler.Task;

public class TaskExecutor implements Runnable {

    Scheduler srtf;
    Scheduler fcfs;
    Scheduler rr;

    public TaskExecutor(ArrayList<Task> SRTFReadyQueue, ArrayList<Task> FCFSReadyQueue, ArrayList<Task> RRReadyQueue, int timeWindow, Scheduler srtf, Scheduler fcfs, Scheduler rr) {
        this.srtf = srtf;
        this.fcfs = fcfs;
        this.rr = rr;
    }

    @Override
    public void run() {
        if (algorithmList.get(0).contains("SRTF")) {
            srtf.execute();
        }

        if (algorithmList.get(0).contains("FCFS")) {
            fcfs.execute();
        }
        if (algorithmList.get(0).contains("RR")) {
            rr.execute();
        }

    }
}
