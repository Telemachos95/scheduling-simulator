package schedulingsimulator.Runnables;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import static schedulingsimulator.GUI.Gui.algorithmList;
import schedulingsimulator.Scheduler.Scheduler;

public class DeadlockDetectorAverageCounter implements Runnable {

    Scheduler srtf;
    Scheduler fcfs;
    Scheduler rr;

    public DeadlockDetectorAverageCounter(Scheduler srtf, Scheduler fcfs, Scheduler rr) {
        this.srtf = srtf;
        this.fcfs = fcfs;
        this.rr = rr;
    }

    @Override
    public void run() {
        try {
            ThreadMXBean tmx = ManagementFactory.getThreadMXBean();
            long[] ids = tmx.findDeadlockedThreads();
            if (ids != null) {
                ThreadInfo[] infos = tmx.getThreadInfo(ids, true, true);
                System.out.println("The following threads are deadlocked:");
                for (ThreadInfo ti : infos) {
                    System.out.println(ti);
                }
            }

            if (algorithmList.get(0).contains("SRTF")) {
                System.out.println("SRTF AvgResponseTime:" + srtf.getAvgResponseTime() + ", " + "AvgWaitingTime:" + srtf.getAvgWaitingTime());
            }
            if (algorithmList.get(0).contains("FCFS")) {
                System.out.println("FCFS AvgResponseTime:" + fcfs.getAvgResponseTime() + ", " + "AvgWaitingTime:" + fcfs.getAvgWaitingTime());
            }
            if (algorithmList.get(0).contains("RR")) {
                System.out.println("RR AvgResponseTime:" + rr.getAvgResponseTime() + ", " + "AvgWaitingTime:" + rr.getAvgWaitingTime());
            }

        } catch (Exception e) {
            System.out.println("Error");
        }
    }
;
}
