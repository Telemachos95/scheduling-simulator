package schedulingsimulator.Runnables;

import java.util.ArrayList;
import java.util.Random;
import schedulingsimulator.Scheduler.Task;

public class TaskGenerator implements Runnable {

    ArrayList<Task> SRTFReadyQueue;
    ArrayList<Task> FCFSReadyQueue;
    ArrayList<Task> RRReadyQueue;

    public TaskGenerator(ArrayList<Task> SRTFReadyQueue, ArrayList<Task> FCFSReadyQueue, ArrayList<Task> RRReadyQueue) {
        this.SRTFReadyQueue = SRTFReadyQueue;
        this.FCFSReadyQueue = FCFSReadyQueue;
        this.RRReadyQueue = RRReadyQueue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            Task t = new Task(new Random().nextInt(100));
            SRTFReadyQueue.add(t);
        }
        for (Task t : SRTFReadyQueue) {
            FCFSReadyQueue.add(new Task(t.getWorkload()));
            RRReadyQueue.add(new Task(t.getWorkload()));
        }

    }
}
