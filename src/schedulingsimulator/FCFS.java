package schedulingsimulator;

import schedulingsimulator.Scheduler.Scheduler;
import schedulingsimulator.Scheduler.Task;

import java.util.ArrayList;

public class FCFS extends Scheduler {

    int counter = 0;

    public FCFS(ArrayList<Task> ReadyQueue) {
        super(ReadyQueue);
    }

    @Override
    public void schedule() {
        int timeElapsed = 0;
        while (true) {
            Task t = this.ReadyQueue.get(0);
            t.execute();
            timeElapsed++;
            if (t.isCompleted()) {
                this.ReadyQueue.remove(0);
                this.logCompletedTask(t, timeElapsed);
                counter++;
                System.out.println("Task(FCFS): " + counter + "(100)");
                break;
            }
        }

    }
}
